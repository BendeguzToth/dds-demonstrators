Build the docs, with sphinx
===========================

To build the docs, use ``make``’ or when make is not installed, call ``sphinx-build`` directly. In all cases, do it from
this directory.

The result can be found in :`__result/html/` (start with `__result/html/index.html`)

With make:
.. code-block:: bash

   > make
   # or
   > make html

Without:
.. code-block:: bash

 > sphinx-build -c doc -b html  -d tmp/sphinx_cache  doc/  __result/html



